import zipfile
from typing import Iterable
import dataclasses
import subprocess
import tempfile
import io
import re
import glob
import json
import pprint
import sys
import io
import os
from jinja2 import Environment, FileSystemLoader


GRADLE_INJECTION = """\n\n\nproject.afterEvaluate {
    configurations.forEach { config ->
        if (config.name == "implementation") {
            config.dependencies.forEach { dep ->
                println("DEP:${dep.group}:${dep.name}:${dep.version}")
            }
        }

    }
}
"""

@dataclasses.dataclass
class JavaPackage:
    group_id: str
    artifact_id: str
    version: str

    def __hash__(self) -> int:
        return hash(self.artifact_id + self.group_id + self.version)

def generate_gitlab_metadata(apk_file_path):
    return {
    "timestamp": "2024-03-06T11:09:43Z",
    "tools": [
      {
        "vendor": "GitLab",
        "name": "APK scanning",
        "version": "0.2.2"
      }
    ],
    "authors": [
      {
        "name": "GitLab",
        "email": "support@gitlab.com"
      }
    ],
    "properties": [
      {
        "name": "gitlab:dependency_scanning:input_file",
        "value": os.path.basename(apk_file_path)
      },
      {
        "name": "gitlab:dependency_scanning:input_file:path",
        "value": apk_file_path
      },
      {
        "name": "gitlab:dependency_scanning:package_manager",
        "value": "Maven"
      },
      {
        "name": "gitlab:dependency_scanning:package_manager:name",
        "value": "Maven"
      },
      {
        "name": "gitlab:meta:schema_version",
        "value": "1"
      }
    ]
  }


PACKAGE_NAME_FROM_FILE_PATH_PATTERN = re.compile(r"META-INF\/(?P<package>.*)\.version")


def get_packages(apk_file: zipfile.ZipFile) -> Iterable[JavaPackage]:
    for file_name in apk_file.namelist():
        if file_name.startswith("META-INF") and file_name.endswith(".version"):
            try:
                version = apk_file.read(file_name).strip().decode()
                group_id, artifact_id = PACKAGE_NAME_FROM_FILE_PATH_PATTERN.findall(file_name)[0].split("_")
                yield JavaPackage(group_id, artifact_id, version)
            except Exception as e:
                print(f"Failed to parse {file_name} with exception: {e}", sys.stderr)
                continue


def get_cyclondx(java_packages: Iterable[JavaPackage]):
    env = Environment(loader=FileSystemLoader("/"))
    template = env.get_template(template_path)
    pom_xml_content = template.render(dependencies=java_packages)
    with tempfile.TemporaryDirectory() as pom_xml_location_directory:
        pom_xml_location = f"{pom_xml_location_directory}/pom.xml"
        with open(pom_xml_location, "w") as pom_xml:
            pom_xml.write(pom_xml_content)
            pom_xml.flush()
            return generate_sbom_from_pom_xml(pom_xml_location)

        

def generate_sbom_from_pom_xml(pom_xml_path: str):
    syft_process = subprocess.Popen(["syft", "scan", f"file:{pom_xml_path}", "-o", "cyclonedx-json "],
                                    stdout=subprocess.PIPE)
    stdout, _ = syft_process.communicate()
    return json.load(io.BytesIO(stdout))


def inject_after_value():
    for file_path in glob.glob("**/*build.gradle.kts"):
      with open(file_path, "w") as build_file:
          if "dependencies" in build_file.read():
              build_file.seek(0, io.SEEK_END)
          build_file.write(GRADLE_INJECTION)
          break
      
def run_build():
    build_process = subprocess.Popen(["gradle", "build", "-m"], stdout=subprocess.PIPE)
    stdout, _ = build_process.communicate()
    try:
        output = stdout.decode()
    except:
        print("Failed tp parse gradle build output", file=sys.stderr)
    return output

def parse_dependencies_from_output(output: str):
    PATTERN = re.compile(r"DEP:(?P<package>.*)")
    for dependency in PATTERN.findall(output):
      try:
        group_id, artifact_id, version = dependency.split(":")
      except:
          print("Failed tp parse dependency line {dependency}", file=sys.stderr)
          continue
      
      yield JavaPackage(group_id, artifact_id, version)
    
    

if __name__ == "__main__":
    found_packages = set()
    file_path, template_path = sys.argv[1], sys.argv[2]
    try:
      found_packages.update(get_packages(zipfile.ZipFile(file_path)))
    except:
        print(f"Failed to get packages from APK file. Using only build.gradle.kts dependencies.", file=sys.stderr)
    inject_after_value()
    output = run_build()
    found_packages.update(parse_dependencies_from_output(output))
    cyclondx = get_cyclondx(list(found_packages))
    cyclondx['metadata'] = generate_gitlab_metadata(file_path)
    with open("gl-sbom-android-maven.cdx.json", "w") as output_cyclondx_file:
        json.dump(cyclondx, output_cyclondx_file)